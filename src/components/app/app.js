import AppInfo from '../app-info/app-info';
import SearchPanel from '../search-panel/search-panel';
import AppFilter from '../app-filter/app-filter';
import EmployeesList from '../employees-list/employees-list';
import EmployeesAddForm from '../employees-add-form/employees-add-form';


function App() {
    
    const data = [
        {name: 'John', salary: 8000, increase: false, id: 1},
        {name: 'Kate', salary: 50000, increase: true, id: 2},
        {name: 'John', salary: 8000, increase: false, id: 3},
        {name: 'John', salary: 8000, increase: false, id: 4},
        {name: 'Kate', salary: 50000, increase: false, id: 5}
    ];

    return (
        <div className="app">
            <AppInfo/>
            <div className="search-panel">
                <SearchPanel/>
                <AppFilter/>
            </div>
            <EmployeesList data={data}/>
            <EmployeesAddForm/>
        </div>
    );
}

export default App;
